
import express from 'express';
import winston from 'winston';
import path from 'path';
import morgan from 'morgan';
import serveStatic from 'serve-static';
import React from 'react';
import { renderToString } from 'react-dom/server';
import axios from 'axios';
import io from 'socket.io';
import ioClient from 'socket.io-client';
import http from 'http';
import App from './src/App';
import { getData } from './lib/coins';

const app = express();

const webServer = http.Server(app);
const ioServer = io(webServer);

app.set('view engine', 'pug');
app.set('views', __dirname);

app.use(morgan('dev'));
app.use(serveStatic(path.join(__dirname, 'dist')));
app.use(serveStatic(path.join(__dirname, 'node_modules/bulma/css')));

app.get('/prices', (req, res) => {
    res.render('src/index', (err, html) => {
        if (err) throw err;
        res.send(html.replace('content', renderToString(<App />)));
    });
});

setInterval(() => {
    getData('coinbase')
        .then(data => ioServer.emit('cbTick', data))
        .catch(err => console.log(err));
}, 10000);

// Disabling this in favor of the socket connection below
// setInterval(() => {
//     getData('btcmarkets')
//         .then(data => ioServer.emit('btcmTick', data))
//         .catch(err => console.log(err));
// }, 10000);

setInterval(() => {
    getData('coinspot')
        .then(data => {
            ioServer.emit('csTick', data)
        })
        .catch(err => console.log(err));
}, 60000);

const socket = ioClient('https://socket.btcmarkets.net', {secure: true, transports: ['websocket'], upgrade: false});
socket.on('connect', () => {
    console.log('Connected to BTCMarkets Socket Server');
    const baseCoins = ['BTC', 'ETH', 'LTC', 'BCH', 'ETC', 'XRP'];
    baseCoins.forEach(baseCoin => socket.emit('join', `Ticker-BTCMarkets-${baseCoin}-AUD`));
});

socket.on('newTicker', data => {
    ioServer.emit('btcmSingle', data);
});

ioServer.on('connection', () => {
    winston.info('Got connection.');

    axios.all([getData('coinbase'), getData('btcmarkets'), getData('coinspot')])
        .then(axios.spread((cbData, btcmData, csData) => {
            ioServer.emit('initialize', { cbData, btcmData, csData });
        }));
});

webServer.listen(3000, () => {
    winston.info('Server is up');
});