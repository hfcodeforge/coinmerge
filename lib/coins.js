import axios from 'axios';

export function getData(type) {
    switch (type) {
        case 'coinbase': return getCbData();
        case 'btcmarkets': return getBtcmData();
        case 'coinspot': return getCsData();
    }
}

const baseCoins = ['BTC', 'ETH', 'LTC', 'BCH'];

function getCbData() {
    const cbBaseUrl = 'https://api.coinbase.com/v2';
    
    return axios.all(baseCoins.map(coin => {
        return axios
            .get(`${cbBaseUrl}/prices/${coin}-AUD/spot`, {
                headers: {
                    'CB-VERSION': '2018-01-09'
                }
            })
            .then(res => res.data)
            .catch(err => console.log(err));
    }))
        .then(responses => {
            return responses.reduce((prices, response) => {
                const data = response.data;
                const key = Object.keys(prices).find(key => key === data.base.toLowerCase());
                const amount = Number(data.amount);
                if (typeof key === 'undefined') prices[data.base.toLowerCase()] = (amount + (amount * 0.0399)).toFixed(2);
                return prices;
            }, {});
        })
        .catch(err => console.log(err));
}

function getBtcmData() {
    const btcmBaseUrl = 'https://api.btcmarkets.net';
    const btcmCoins = baseCoins.concat(['ETC', 'XRP']);
    
    return axios.all(btcmCoins.map(coin => {
        return axios.get(`${btcmBaseUrl}/market/${coin}/AUD/tick`)
            .then(res => res.data)
            .catch(err => console.log(err));
    }))
        .then(responses => {
            return responses.reduce((prices, response) => {
                const key = Object.keys(prices).find(key => key === response.instrument.toLowerCase());
                const amount = response.lastPrice;
                if (typeof key === 'undefined') prices[response.instrument.toLowerCase()] = (amount + (amount * 0.0075)).toFixed(2)
                return prices;
            }, {});
        })
        .catch(err => console.log(err));
}

function getCsData() {
    const csBaseUrl = 'https://www.coinspot.com.au/pubapi/latest';

    return axios.get(csBaseUrl)
        .then(res => {
            const btcLast = Number(res.data.prices.btc.last);
            const ltcLast = Number(res.data.prices.ltc.last);

            return {
                btc: (btcLast + (btcLast * 0.02)).toFixed(2),
                ltc: (ltcLast + (ltcLast * 0.02)).toFixed(2),
            };
        })
        .catch(err => console.log(err));
}