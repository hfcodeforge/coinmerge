# CoinMerge
A simple tool to check spot prices on the following crypto-markets:
- Coinbase
- BTCMarkets
- Coinspot

## Requirements
- VPS (preferably Linux) with terminal access via SSH
- git (optional) to clone this repository
- NodeJS to install dependencies

## Installation
- Clone or download this repository
- Using terminal, navigate to the project directory and execute `npm install` to install project dependencies. Make sure you are running this with admin/root privileges.
- After installation, execute the following commands in sequence:
    - `npm run clean` - cleans build files in case you need to rebuild it
    - `npm run build-server` - builds the NodeJS web server
    - `npm run build-prod` - builds the ReactJS front-end files
    - `npm run copy-files` - copy assets
- Install [PM2](https://github.com/Unitech/pm2) to manage your Node application as a service. To do this, you can simply just execute `npm install pm2 -g`.
- After installing PM2, on the project directory execute `pm2 start ./build/index.js --name="coinmerge"`.

## F.A.Q.
1. _I did the installation above, but I don't think it's working. What do I do?_

Check the applications logs by issuing `pm2 logs coinmerge` on the terminal to see if there are any errors. Another thing to check is if your reverse proxy works. As of this moment, I have only configured Nginx with this project and I've never tried Apache. To check if you have right reverse proxy settings, refer to this [link](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-14-04#set-up-reverse-proxy-server). This application is running on port 3000, so just substitute that on 8080 references on that link.

If you have other questions, contact the coder [here](https://hackforums.net/private.php?action=send&uid=826234).