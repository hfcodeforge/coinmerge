import React, { Component } from 'react';
import Header from './Header';
import { getData } from '../lib/coins';
import axios from 'axios';
import config from '../config';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0,
            sources: [
                { name: 'Coinbase', data: {} },
                { name: 'BTCMarkets', data:{} },
                { name: 'Coinspot', data: {} }
            ]
        };
    }

    componentDidMount() {
        const socket = io.connect(config.uri);
        socket.on('initialize', data => {
            this.setState({
                sources: [
                    {
                        ...this.state.sources[0],
                        data: data.cbData
                    },
                    {
                        ...this.state.sources[1],
                        data: data.btcmData
                    },
                    {
                        ...this.state.sources[2],
                        data: data.csData
                    }
                ]
            });
        });

        socket.on('cbTick', data => {
            const source = this.state.sources.find(_source => _source.name === 'Coinbase');
            const sourceIndex = this.state.sources.findIndex(_source => _source.name === 'Coinbase');
            this.setState({
                sources: [
                    ...this.state.sources.slice(0, sourceIndex),
                    {
                        ...source,
                        data
                    },
                    ...this.state.sources.slice(sourceIndex + 1)
                ]
            });
        });

        socket.on('btcmTick', data => {
            const source = this.state.sources.find(_source => _source.name === 'BTCMarkets');
            const sourceIndex = this.state.sources.findIndex(_source => _source.name === 'BTCMarkets');
            this.setState({
                sources: [
                    ...this.state.sources.slice(0, sourceIndex),
                    {
                        ...source,
                        data
                    },
                    ...this.state.sources.slice(sourceIndex + 1)
                ]
            });
        });

        socket.on('csTick', data => {
            const source = this.state.sources.find(_source => _source.name === 'Coinspot');
            const sourceIndex = this.state.sources.findIndex(_source => _source.name === 'Coinspot');
            this.setState({
                sources: [
                    ...this.state.sources.slice(0, sourceIndex),
                    {
                        ...source,
                        data
                    },
                    ...this.state.sources.slice(sourceIndex + 1)
                ]
            });
        });

        socket.on('btcmSingle', data => {
            const source = this.state.sources.find(_source => _source.name === 'BTCMarkets');
            const sourceIndex = this.state.sources.findIndex(_source => _source.name === 'BTCMarkets');
            const lastPrice = data.lastPrice/100000000;

            this.setState({
                sources: [
                    ...this.state.sources.slice(0, sourceIndex),
                    {
                        ...source,
                        data: {
                            ...source.data,
                            [data.instrument.toLowerCase()]: (lastPrice + (lastPrice * 0.0075)).toFixed(2)
                        }
                    },
                    ...this.state.sources.slice(sourceIndex + 1)
                ]
            });
        });
    }

    render() {
        const columnHeaders = this.state.sources
            .map(source => Object.keys(source.data))
            .reduce((headers, coinKeys) => {
                return coinKeys.reduce((_headers, coinKey) => {
                    if (typeof _headers.find(header => header === coinKey.toUpperCase()) === 'undefined') return _headers.concat([coinKey.toUpperCase()]);
                    return _headers;
                }, headers);
            }, [])
            .sort((a, b) => {
                if (a > b) return 1;
                if (a < b) return -1;
                if (a == b) return 0;
            });

            const coinValues = this.state.sources
                .reduce((values, source) => {
                    return Object.keys(source.data).reduce((_values, dataKey) => {
                        _values[dataKey] = [
                            ...(_values[dataKey] || []),
                            source.data[dataKey]
                        ]

                        return _values;
                    }, values);
                }, {});

            const highsAndLows = Object.keys(coinValues).reduce((hnls, key) => {
                const high = coinValues[key].sort((a, b) => {
                    if (a > b) return -1;
                    if (a < b) return 1;
                    if (a == b) return 0;
                })[0];

                const low = coinValues[key].sort((a, b) => {
                    if (a > b) return 1;
                    if (a < b) return -1;
                    if (a == b) return 0;
                })[0];

                return {
                    ...hnls,
                    [`${key}-high`]: high,
                    [`${key}-low`]: low,
                    [`${key}-diff`]: high - low
                }
            }, {});

        return (
            <div>
                <section className="section">
                    <div className="container is-fluid">
                        <h1 className="title">Crypto Prices</h1>
                        <h2 className="subtitle">Latest spot price in AUD</h2>
                        <table className="table is-narrow is-striped is-fullwidth is-bordered">
                            <thead>
                                <tr>
                                    <td>Exchange</td>
                                    {columnHeaders.map((columnHeader, index) => (
                                        <td key={`column-${index}`}>{columnHeader}</td>
                                    ))}
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.sources
                                        .map((source, index) => (
                                            <tr key={`row-${index}`}>
                                                <td>{source.name}</td>
                                                {columnHeaders.map((columnHeader, hIndex) => (
                                                    <td key={`row-${index}-${hIndex}`}>
                                                        {source.data[columnHeader.toLowerCase()] || 'N/A'}
                                                        { source.data[columnHeader.toLowerCase()] === highsAndLows[`${columnHeader.toLowerCase()}-high`] &&
                                                            <span className="tag is-success is-pulled-right">Highest</span>
                                                        }
                                                        { (source.data[columnHeader.toLowerCase()] === highsAndLows[`${columnHeader.toLowerCase()}-low`] && 
                                                            highsAndLows[`${columnHeader.toLowerCase()}-low`] !== highsAndLows[`${columnHeader.toLowerCase()}-high`]) &&
                                                            <span className="tag is-danger is-pulled-right">Lowest</span>
                                                        }
                                                    </td>
                                                ))}
                                            </tr>
                                        ))
                                }
                                <tr>
                                    <td>Difference</td>
                                    {columnHeaders.map((columnHeader, hIndex) => (
                                        <td key={`diff-row-${hIndex}`}>{highsAndLows[`${columnHeader.toLowerCase()}-diff`].toFixed(2)}</td>
                                    ))}
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        );
    }
}

export default App;