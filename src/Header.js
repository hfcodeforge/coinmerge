import React from 'react';

const Header = () => (
    <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
            <a className="navbar-item" href="/home">
                CoinMerge
            </a>

            <button className="button navbar-burger">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
    </nav>
);

export default Header;